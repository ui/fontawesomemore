import os
import os.path
import sys
import subprocess
from lxml import etree
from configparser import ConfigParser

FONTFORGE = os.environ.get("FONTFORGE", "fontforge")
INKSCAPE = os.environ.get("INKSCAPE", "inkscape")

BUILD_DIR = "build"
INI = "font.ini"
SVGTEMPLATE = "templates/base.svg"
CSSTEMPLATE = "templates/base.css"
JSTEMPLATE = "templates/base.js"
MERGED = os.path.join(BUILD_DIR, "1.merged.svg")
GENFONT = os.path.join(BUILD_DIR, "2.svgfont.svg")
CLEANFONT = os.path.join(BUILD_DIR, "3.cleanfont.svg")
FINALFONT = os.path.join("fonts", "fontawesomemore-webfont.svg")
FINALCSS = os.path.join("css", "font-awesomemore.css")
JSHOMEPAGE = os.path.join("homepage/icons.js")

INTERMEDIATE = "gen-font.svg"

SVG_NS = '{http://www.w3.org/2000/svg}'
INKSCAPE_NS = "{http://www.inkscape.org/namespaces/inkscape}"


def setup_build():
    os.makedirs("./build", exist_ok=True)


def remove_svg_font(tree):
    root = tree.getroot()
    elements = tree.findall(f'{SVG_NS}defs/{SVG_NS}font')
    for e in elements:
        print("  Remove embeded font")
        e.getparent().remove(e)


def extract_glyphs():
    tree = etree.parse(FINALFONT)
    remove_svg_font(tree)
    root = tree.getroot()

    # Create a template
    elements = tree.findall(f'{SVG_NS}g')
    for e in elements:
        e.getparent().remove(e)

    # Generate each glyphs
    for e in elements:
        name = e.get("id")
        print("  Extract", name)
        try:
            root.append(e)
            filename = f"glyphs/{name}.svg"
            tree.write(filename, pretty_print=True, xml_declaration=True, encoding="utf-8")
        finally:
            e.getparent().remove(e)


def merge_glyphs(input_name, output_name):
    tree = etree.parse(input_name)
    remove_svg_font(tree)
    root = tree.getroot()
    glyphs = get_glyths_config()

    # Create a template
    elements = tree.findall(f'{SVG_NS}g')
    for e in elements:
        e.getparent().remove(e)

    def get_glyph_group(tree2):
        elements = tree2.findall(f'{SVG_NS}g')
        for g in elements:
            return g
        raise RuntimeError("Glyph group not found")

    def get_duo_glyph_group(tree2, label):
        elements = tree2.findall(f'{SVG_NS}g')
        for g in elements:
            if g.get(f"{INKSCAPE_NS}label") == label:
                return g
        raise RuntimeError(f"Glyph group not found '{label}'")

    def append_glyph(root, name, g):
        root.append(g)
        g.set("id", name)
        code = glyphs.get(name, "xxxx")
        char = chr(int(code, 16))
        g.set(f"{INKSCAPE_NS}label", "GlyphLayer-%s" % char)

    # Open and append glyphs
    for num, filename in enumerate(sorted(os.listdir("glyphs"))):
        name, ext = os.path.splitext(filename)
        if filename.startswith("_") or ext != ".svg":
            print("  Skip", filename)
            continue
        print("  Open", filename)
        tree2 = etree.parse(f"glyphs/{filename}")
        root2 = tree2.getroot()

        if name.endswith("#duo"):
            g = get_duo_glyph_group(root2, "primary")
            append_glyph(root, f"{name}#primary", g)
            g = get_duo_glyph_group(root2, "secondary")
            append_glyph(root, f"{name}#secondary", g)
        else:
            g = get_glyph_group(root2)
            append_glyph(root, name, g)

    print("  Save", output_name)
    tree.write(output_name, pretty_print=True, xml_declaration=True, encoding="utf-8")


def cleanup_font(input_name, output_name):
    tree = etree.parse(input_name)
    root = tree.getroot()

    # Patch the font name
    fontFaces = tree.findall(f'{SVG_NS}defs/{SVG_NS}font/{SVG_NS}font-face')
    for fontFace in fontFaces:
        fontFace.set("font-family", "FontAwesomemore")
        break

    # Remove the group content
    name2code = {}
    code2name = {}
    groups = tree.findall(f'{SVG_NS}g')
    for group in groups:
        group.getparent().remove(group)
        name = group.get("id")
        code = group.get(f"{INKSCAPE_NS}label")
        if code.startswith("GlyphLayer-"):
            code = code[11:]
        name2code[name] = code
        code2name[code] = name

    # Check that all glyphs are part of the font
    glyphs = tree.findall(f'{SVG_NS}defs/{SVG_NS}font/{SVG_NS}glyph')
    for glyph in glyphs:
        # patch the id to have a ref to the original name
        code = glyph.get("unicode")
        glyph.set("id", code2name[code])
        d = glyph.get("d")
        if d is None or d == "":
            print(f"  WARNING: Glyph '{name}' in font is empty!!!")

    print("  Save", output_name)
    tree.write(output_name, pretty_print=True, xml_declaration=True, encoding="utf-8")


def glyph_to_icon_name(name):
    return name.split("#")[0]


def generate_css(input_name, output_name):
    # Get template
    with open(CSSTEMPLATE, "rt") as f:
        template = f.read()

    # Gen css
    glyphs = get_glyths_config()
    with open(output_name, "wt") as css:
        css.write(template)
        print(f"  Unicode  Name")
        for name, code in glyphs.items():
            selector = "before"
            hexa = f"\\{code}"
            print(f"  {hexa:>7}  {name}")
            # make sure to override font if it is a name from fontawesome
            important = " !important" if name.startswith("fa-") else ""
            icon_name = f"fa.{name}"
            if "#duo#" in name:
                n = glyph_to_icon_name(name)
                icon_name = f"fad.{n}"
                if name.endswith("#secondary"):
                    selector = "after"
            css.write(f"""\
.{icon_name}::{selector} {{
  font-family: FontAwesomemore{important};
  font-weight: 400{important};
  content: "{hexa}"{important};
}}

""")


def generate_homepage(input_name):
    tree = etree.parse(input_name)
    root = tree.getroot()

    glyph_content = []

    # Gen css
    icons = []
    glyphs = get_glyths_config()
    for name, code in glyphs.items():
        if "#duo#" in name:
            if name.endswith("#secondary"):
                continue
            n = glyph_to_icon_name(name)
            icons.append((n, "fad"))
        else:
            icons.append((name, "fa"))

    icons = sorted(icons)
    for name, style in icons:
        glyph_content.append(f"\t'{style} {name}'")

    # Get template
    with open(JSTEMPLATE, "rt") as f:
        template = f.read()

    # Gen output
    with open(JSHOMEPAGE, "wt") as html:
        text = ",\n".join(glyph_content)
        content = template.replace("{{glyphs}}", text)
        html.write(content)


def execute(cmd, expected_output=None):
    ret = subprocess.call(cmd)
    # ret = os.system(" ".join(cmd))
    if ret != 0:
        print(f"Failed with return code: {ret}")
        return False
    if expected_output is not None and not os.path.exists(expected_output):
        print(f"File {os.path.abspath(FINALFONT)} was not generated")
        return False
    return True


def generate_fonts(input_name):
    cmd = [FONTFORGE,
           "-lang=ff",
           "-script",
           os.path.abspath("convert.pe"),
           os.path.abspath(input_name)]
    if not execute(cmd, expected_output=os.path.abspath(FINALFONT)):
        sys.exit(-3)


def get_glyths_config():
    ini_filename = os.path.abspath(INI)
    config = ConfigParser()
    config.read(ini_filename)
    if not config.has_section("glyphs"):
        config["glyphs"] = {}
    glyphs = config["glyphs"]
    return glyphs


def register_glyphs():
    """Register new glyphs with private unicode.

    Read the 'glyphs' directory, and register unicode
    code point for each new  SVG files not already registered.
    """
    ini_filename = os.path.abspath(INI)
    config = ConfigParser()
    config.read(ini_filename)
    if not config.has_section("glyphs"):
        config["glyphs"] = {}
    glyphs = config["glyphs"]

    unicodes = set()
    for g, u in glyphs.items():
        unicodes.add(u)

    names = set()
    for num, filename in enumerate(sorted(os.listdir("glyphs"))):
        name, ext = os.path.splitext(filename)
        if filename.startswith("_") or ext != ".svg":
            print("  Skip", filename)
            continue
        names.add(name)

    current_unicode = 0xf000
    def alloc_unicode():
        nonlocal current_unicode, unicodes
        while True:
            text = "%04x" % current_unicode
            if text not in unicodes:
                current_unicode = current_unicode + 1
                return text
            current_unicode = current_unicode + 1
        return current_unicode

    def register_glyph(name):
        if name not in glyphs:
            u = alloc_unicode()
            glyphs[name] = u
            unicodes.add(u)

    for n in sorted(names):
        if n.endswith("#duo"):
            register_glyph(f"{n}#primary")
            register_glyph(f"{n}#secondary")
        else:
            register_glyph(n)

    with open(ini_filename, 'wt') as f:
        config.write(f)


def build_font():
    print("================================")
    print("0/8 SETUP BUILD")
    setup_build()
    print("================================")
    print("1/8 REGISTER GLYPHS")
    register_glyphs()
    print("================================")
    print("2/8 MERGE GLYPHS")
    merge_glyphs(SVGTEMPLATE, MERGED)
    print("================================")
    print("3/8 GENERATE FONT")
    cmd = [INKSCAPE,
           "--verb=org.inkscape.typography.layers_to_svg_font.noprefs",
           os.path.abspath(MERGED),
           "-o", os.path.abspath(GENFONT),
           "--with-gui",
           "--batch-process"]
    if not execute(cmd, expected_output=os.path.abspath(GENFONT)):
        sys.exit(-1)
    print("================================")
    print("4/8 CLEANUP FONT")
    cleanup_font(GENFONT, CLEANFONT)

    print("================================")
    print("5/8 PLAIN SVG")
    cmd = [INKSCAPE,
           "--export-plain-svg",
           os.path.abspath(CLEANFONT),
           "-o", os.path.abspath(FINALFONT),
           "--batch-process"]
    if not execute(cmd, expected_output=os.path.abspath(FINALFONT)):
        sys.exit(-2)
    print("================================")
    print("6/8 GENERATE CSS")
    generate_css(FINALFONT, FINALCSS)

    print("================================")
    print("7/8 GENERATE HOMEPAGE")
    generate_homepage(FINALFONT)

    print("================================")
    print("8/8 GENERATE FONTS")
    generate_fonts(FINALFONT)

    print("================================")


def main():
    cmd = None if len(sys.argv) != 2 else sys.argv[1]
    if len(sys.argv) != 2:
        print("Commands:")
        print("   clean:     clean up global file")
        print("   extract:   extract glyphs from global file")
        print("   merge:     merge glyphs into a glabal file")
    elif cmd == "clean":
        cleanup_font()
    elif cmd == "extract":
        extract_glyphs()
    elif cmd == "register":
        register_glyphs()
    elif cmd == "merge":
        merge_glyphs()
    elif cmd == "fonts":
        generate_fonts(FINALFONT)
    elif cmd == "html":
        generate_homepage(FINALFONT)
    elif cmd == "build":
        build_font()
    else:
        raise Exception(f"Unknown command '{cmd}'")
    print("OK")


if __name__ == "__main__":
    main()

