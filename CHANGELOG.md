# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

## [1.19.0](https://gitlab.esrf.fr/ui/fontawesomemore/compare/v1.18.0...v1.19.0) (2024-08-20)

## [1.18.0](https://gitlab.esrf.fr/ui/fontawesomemore/compare/v1.17.0...v1.18.0) (2024-08-16)

## 1.17.0 (2024-05-06)

## [1.16.0](https://gitlab.esrf.fr/ui/fontawesomemore/compare/v1.15.0...v1.16.0) (2022-11-23)

## [1.15.0](https://gitlab.esrf.fr/ui/fontawesomemore/compare/v1.14.0...v1.15.0) (2022-11-10)

## 1.14.0 (2022-07-18)

## 1.13.0 (2022-07-13)

## [1.12.0](https://gitlab.esrf.fr/ui/fontawesomemore/compare/v1.11.0...v1.12.0) (2022-07-11)

- Added support to home made duo tone `fad`

## [1.11.0](https://gitlab.esrf.fr/ui/fontawesomemore/compare/v1.9.0...v1.11.0) (2022-07-06)

## [1.10.0](https://gitlab.esrf.fr/ui/fontawesomemore/compare/v1.9.0...v1.10.0) (2022-07-06)

## [1.9.0](https://gitlab.esrf.fr/ui/fontawesomemore/compare/v1.8.0...v1.9.0) (2022-06-28)

## [1.8.0](https://gitlab.esrf.fr/ui/fontawesomemore/compare/v1.7.0...v1.8.0) (2022-06-28)

## [1.7.0](https://gitlab.esrf.fr/ui/fontawesomemore/compare/v1.6.0...v1.7.0) (2022-05-12)

## [1.6.0](https://gitlab.esrf.fr/ui/fontawesomemore/compare/v1.5.0...v1.6.0) (2022-05-12)

## [1.5.0](https://gitlab.esrf.fr/ui/fontawesomemore/compare/v1.4.0...v1.5.0) (2022-05-11)

## 1.4.0 (2022-05-04)

## 1.3.0 (2022-03-02)

## 1.3.0 (2022-03-02)
