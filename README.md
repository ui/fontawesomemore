
Project to provided icons to our web based UI software.

[Explore the latest icons from the master](https://ui.gitlab-pages.esrf.fr/fontawesomemore/).

![Few glyphs](./screenshot.png)

It is based on fontawesome concept, in which each icon is a glyph
from a dedicated font. A CSS file to the conversion from class name
to the right glyph to use.

It is an extension to fontawesome. The fontawesome styles have to be
used.

Source
======

Most of the icons are derivative work from FontAweSome (https://fontawesome.com/)
under Creative Commons Attribution-Share Alike 3.0.

https://commons.wikimedia.org/wiki/Category:Font_Awesome

Dependencies
============

```
wget https://inkscape.org/gallery/item/29256/Inkscape-3bf5ae0-x86_64.AppImage
chmod +x Inkscape-3bf5ae0-x86_64.AppImage
export INKSCAPE=./Inkscape-3bf5ae0-x86_64.AppImage

wget https://github.com/fontforge/fontforge/releases/download/20201107/FontForge-2020-11-07-21ad4a1-x86_64.AppImage
chmod +x FontForge-2020-11-07-21ad4a1-x86_64.AppImage
export FONTFORGE=./FontForge-2020-11-07-21ad4a1-x86_64.AppImage
```

Build
=====

```
python svgfont.py build
```

Release
=======

```
npm run release
```

Publish
=======

```
npm publish
```

