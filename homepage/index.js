
var selectedStyle = 'light';
var selectedIconName = 'light';

const duoStyle ={
	duo1: "--fa-primary-color: gold;",
	duo2: "--fa-secondary-color: limegreen;",
	duo3: "--fa-primary-color: peru; --fa-secondary-color: linen; --fa-secondary-opacity: 1.0;",
	duo4: "--fa-secondary-opacity: 1.0; --fa-primary-color: rgb(4, 56, 161); --fa-secondary-color: rgb(108, 108, 108);",
}

const styleNameBgVariant ={
	light: "light",
	dark: "dark",
	duo1: "light",
	duo2: "light",
	duo3: "dark",
	duo4: "light",
}


function Icon(iconName) {
	return `
	<li>
		<button type="button" class="btn btn-light" onClick="showDetail('${iconName}')">
		<i class="${iconName} fa-2x"></i>
		<span class="icon-name">${iconName}</span>
		</button>
	</li>`;
}

function ButtonStyleSelection(iconName, styleName, selectedStyle, style) {
	const bgVariant = styleNameBgVariant[styleName];
	const btnVariant = selectedStyle === styleName ? 'primary' : bgVariant;
	return `
		<button type="button" class="btn btn-sm border-4 border-${btnVariant} btn-${bgVariant}" onClick="selectStyle('${styleName}')">
			<i class="${iconName} fa-fw" style="${style}"></i>
		</button>
	`;
}

function StyleSelector(iconName, selectedStyle) {
	const isDuo = iconName.indexOf("fad ") === 0;
	var result = '';
	result += ButtonStyleSelection(iconName, 'light', selectedStyle, '');
	result += ButtonStyleSelection(iconName, 'dark', selectedStyle, '');
	if (isDuo) {
		result += ButtonStyleSelection(iconName, 'duo1', selectedStyle, duoStyle['duo1']);
		result += ButtonStyleSelection(iconName, 'duo2', selectedStyle, duoStyle['duo2']);
		result += ButtonStyleSelection(iconName, 'duo3', selectedStyle, duoStyle['duo3']);
		result += ButtonStyleSelection(iconName, 'duo4', selectedStyle, duoStyle['duo4']);
	}
	return result;
}

function LightDetail(iconName) {
	return `
	<div class="card">
		<div class="card-body">
		<h5 class="card-title">Light</h5>
		<i class="${iconName} fa-fw"></i>
		<i class="${iconName} fa-2x"></i>
		<i class="${iconName} fa-3x"></i>
		<i class="${iconName} fa-4x"></i>
		</div>
	</div>
	`;
}

function DarkDetail(iconName) {
	return `
	<div class="card bg-dark">
		<div class="card-body text-light">
		<h5 class="card-title">Dark</h5>
		<i class="${iconName} fa-fw"></i>
		<i class="${iconName} fa-2x"></i>
		<i class="${iconName} fa-3x"></i>
		<i class="${iconName} fa-4x"></i>
		</div>
	</div>
	`;
}

function DuoDetail(iconName, styleName) {
	const style = duoStyle[styleName];
	const bgVariant = styleNameBgVariant[styleName];
	const textVariant = bgVariant === 'dark' ? 'light' : 'dark';
	return `
	<div class="card bg-${bgVariant}">
		<div class="card-body">
		<h5 class="card-title text-${textVariant}">Custom style</h5>
		<i class="${iconName} fa-fw" style="${style}"></i>
		<i class="${iconName} fa-2x" style="${style}"></i>
		<i class="${iconName} fa-3x" style="${style}"></i>
		<i class="${iconName} fa-4x" style="${style}"></i>
		</div>
	</div>
	<div class="font-monospace">
	${style.replaceAll('; ', ';<br />')}
	</div>
`;
}

function Duo1Detail(iconName, style) {
	return DuoDetail(iconName, 'duo1');
}

function Duo2Detail(iconName, style) {
	return DuoDetail(iconName, 'duo2');
}

function Duo3Detail(iconName, style) {
	return DuoDetail(iconName, 'duo3');
}

function Duo4Detail(iconName, style) {
	return DuoDetail(iconName, 'duo4');
}

function onLoad() {
	console.log("onload")
	const container = document.getElementById('container');
	var html = ""
	ICON_NAMES.forEach((iconName) => {
		html += Icon(iconName);
	});
	container.innerHTML = html;

	const detail = document.getElementById('detail');
	detail.addEventListener("click", hideOnClickOutside, false);
}

document.addEventListener("DOMContentLoaded", onLoad);

function selectStyle(style) {
	console.log("aaaa")
	selectedStyle = style;
	updateDetail();
}

function showDetail(iconName) {
	selectedIconName = iconName;
	updateDetail();
}

function updateDetail() {
	const detail = document.getElementById('detail');
	detail.style.display = "block";
	title = detail.getElementsByClassName("modal-title")[0];
	title.textContent = selectedIconName;
	const isDuo = selectedIconName.indexOf("fad ") === 0;

	var styleName = selectedStyle;
	if (!isDuo && selectedStyle.indexOf('duo') === 0) {
		styleName = 'light'
	}

	const selection = document.getElementById('detail-style-selection');
	selection.innerHTML = StyleSelector(selectedIconName, styleName);

	const components = {
		light: LightDetail,
		dark: DarkDetail,
		duo1: Duo1Detail,
		duo2: Duo2Detail,
		duo3: Duo3Detail,
		duo4: Duo4Detail,
	};
	var content = document.getElementById('detail-content');
	content.innerHTML = components[styleName](selectedIconName);
}

function hideOnClickOutside(event) {
	if (event.target.closest("button")) {
		return;
	}
	if (!event.target.closest(".modal-dialog")) {
		hideDetail()
	}
}

function hideDetail() {
	console.log("hide")
	const detail = document.getElementById('detail');
	detail.style.display = "none";
}
